import React from "react";
import Docxtemplater from "docxtemplater";
import PizZip from "pizzip";
import PizZipUtils from "pizzip/utils/index.js";
import { saveAs } from "file-saver";
import {my_cvs} from "./mock/cv"
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}

export const App = class App extends React.Component {
  render() {
    const user = {
      cvs: [], email: "pauline.febvey@devoteam.com",
      nom: "Bouizi",
      pole: "77cdc620-9381-11ea-ab50-0da1f4a4338f",
      prenom: "Yassir",
      role: "USER"}
    const  my_cv=my_cvs.cvs[0]
    //console.log(my_cv.apropos.anexperience)
    const generateDocument = () => {
      loadFile("http://localhost:3000/cv_FR_anonyme_portrait.docx", function(
        error,
        content
      ) {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater().loadZip(zip);
        doc.setData({
          first_name: user.nom,
          last_name: user.prenom,
          years_of_experience:my_cv.apropos.anexperience,
          formations: my_cv.formations,
          certifications: my_cv.certifications,
          description: my_cv.apropos.description,
          experiences: my_cv.experiences,
          langues:my_cv.langues
        });
        try {
          // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
          doc.render();
        } catch (error) {
          // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function(
                error,
                key
              ) {
                error[key] = value[key];
                return error;
              },
              {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));

          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function(error) {
                return error.properties.explanation;
              })
              .join("\n");
            console.log("errorMessages", errorMessages);
            // errorMessages is a humanly readable message looking like this :
            // 'The tag beginning with "foobar" is unopened'
          }
          throw error;
        }
        var out = doc.getZip().generate({
          type: "blob",
          mimeType:
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        }); //Output the document using Data-URI
        saveAs(out, "output.docx");
      });
    };

    return (
      <div className="p-2">
        <button onClick={generateDocument}>Generate document</button>
      </div>
    );
  }
};
