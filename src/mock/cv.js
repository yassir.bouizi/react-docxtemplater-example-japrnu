export const my_cvs = {
  cvs: [
    {
      competences: {
        otherCompetences: [{ label: null }, { label: null }, { label: null }, { label: null }, { label: null }],
        skills: [],
        mandatoryCompetences: [
          { note: 9, label: "Agile" },
          { note: 9, label: "Software Craftsmanship" },
          { note: 8, label: "Java" },
          { note: 7, label: "JavaScript" },
          { note: 7, label: "React native" },
        ],
      },
      langues: [
        { niveau: "natal", commentaire: null, langue: 1 },
        { niveau: "natal", commentaire: null, langue: 7 },
        { niveau: "bilingue", commentaire: null, langue: 2 },
        { niveau: null, commentaire: null, langue: null },
        { niveau: null, commentaire: null, langue: null },
      ],
      updateDate: "2021-02-03T14:13:24.961Z",
      formations: [
        {
          anneesFormationfin: 2010,
          anneesFormationdeb: 2009,
          anneeCourante: false,
          ecole: "Université de Pau et des Pays de l'Adour",
          diplome: "Master 2 - Technologie de l'Internet parcours Génie logiciel et systèmes distribués",
        },
      ],
      loisirs: [{ loisir: "Surf" }, { loisir: "Course à pied " }, { loisir: null }, { loisir: null }, { loisir: null }],
      id: 1,
      certifications: [
        { anneescertif: 2020, certif: "Blockchain Basics", organisme: "University at Buffalo" },
        { anneescertif: 2020, certif: "Impact investing ", organisme: "ESSEC Business School" },
        { anneescertif: 2020, certif: "Product management", organisme: "Meraki" },
        { anneescertif: 2019, certif: "SAFe 4 Program Consultant (SPC)", organisme: "Scaled Agile, Inc." },
        {
          anneescertif: 2018,
          certif: "APPLYING THE PRINCIPLES OF FLOW - BY DON REINERTSEN",
          organisme: "Lean Knaban France ",
        },
        { anneescertif: 2018, certif: "Agile Meets Design Thinking", organisme: "University Of Virginia" },
        { anneescertif: 2017, certif: "Réussir le changement", organisme: "ESSEC Business School" },
      ],
      apropos: {
        anexperience: 11,
        description:
          "Coach, développeur, architecte et consultant. J’accompagne nos équipes et celles de nos clients dans l’utilisation de tous les leviers techniques et organisationnels, dans le but d’améliorer leurs efficacités, réduire le time to market et satisfaire les besoins métiers.",
        titre: "Head Of Tech",
      },
      experiences: [
        {
          entreprise: "Axance by Devoteam",
          anneesExperiencefin: null,
          projet: " ",
          moisExperiencefin: null,
          moisExperiencedeb: 2,
          technique: " ",
          description: " ",
          poste: "Head of Tech",
          anneeCourante: true,
          anneesExperiencedeb: 2020,
        },
        {
          entreprise: "Societe Generale Corporate and Investment Banking - SGCIB",
          anneesExperiencefin: 2020,
          projet: " ",
          moisExperiencefin: 11,
          moisExperiencedeb: 2,
          technique:
            "Coaching de deux tribus de la banque de finance et investissement Société Générale (SGCIB)\n10 feature teams à paris + 7 feature teams à bangalore\n ",
          description:
            "- Audit et préconisation d’organisation\n- Mise en place des pratiques flow: management visuel, limiter les encours, Culture de la mesure (Valeur, coût du délai, Lead Time, Cycle time et Vélocité)\n- Accompagnement dans la définition des OKR \n- Formation et coaching des Product Owners et des Chapter leaders\n- Mise en place des principes d’ownership et de business value chain \n- Accompagnement à la conduite du changement\n- Facilitation d’ateliers (priorisation, estimation, storymap, eventstorming, etc.)\n- Formation est accompagnement sur les pratiques Craftsmanship, XP\n- Mise en place d’atelier de revue d’architectures et de revue de code inter-équipe\n- Revue et réorganisation des boards, des backlog et des roadmap",
          poste: "Lean/Agile & Software Craftsman Coach",
          anneeCourante: false,
          anneesExperiencedeb: 2018,
        },
        {
          entreprise: "Legaliform",
          anneesExperiencefin: 2018,
          projet: " ",
          moisExperiencefin: 3,
          moisExperiencedeb: 2,
          technique:
            " Mise en place des pratiques: \n* Lean/Agile\n* CI/CD\n* Lean Startup/Design sprint\n* Growth hacking/SEO \n        * Numéro 1 google sur le mot clé principale\n        * Plus de 400 prospects issue de la stratégie inbound marketing",
          description:
            "Legaliform est une solution SaaS de veille réglementaire HSE: Identification des textes et exigences réglementaires par des juristes experts en HSE, Alerting, gestion de la conformité, etc.\n\nEn tant que CTO, j'étais responsable d’une équipe technique full remote, de la stratégie technique et de l'architecture de la plate-forme.\n\n",
          poste: "Co-founder & CTO",
          anneeCourante: false,
          anneesExperiencedeb: 2017,
        },
        {
          entreprise: "OCTO Technology",
          anneesExperiencefin: 2017,
          projet: " ",
          moisExperiencefin: 2,
          moisExperiencedeb: 4,
          technique: " ",
          description:
            "* Senior Architect JAVA/JEE.\n   * Full stack Developer:\n   * Spring Stack Expert : spring-core , spring-security , spring-mvc , spring-data, spring-batch.\n   * JS framework : angularJS\n   * Monitoring : ELK\n   * Build JEE Automation expert : Maven , Nexus , Jenkins.\n   * Cloud: AWS (EC2, Beanstalk, RDS)\n   * Nosql/BigData (MongoDB , Elasticsearch,Redis, Hadoop)\n   * Dataviz : Tableau, Kibana\n   * Agile Practitioner : Scrum, Kanban, Lean, CraftmanShip, TDD, BDD, Pair Programming, Code Review.\n\nUX Design:\n-Mockup - Balsamiq\n-Test Utilisateur: Guerilla Usability Testing\n\n* Architecture :\n   * Experience in building complex software architecture .(High Availabilty , performance , scalability)\n   * Experience in auditing systems/applications, stress testing, profiling (jmeter, gatling, VisualVM, Appdynamics, Newrelic)\n\n   * Coaching (people or/and technical coaching)\n   * Good problem analyzer an resolver",
          poste: "IT Consultant",
          anneeCourante: false,
          anneesExperiencedeb: 2014,
        },
        {
          entreprise: "Orange",
          anneesExperiencefin: 2014,
          projet: " Refonte d’un référentiel de livraison Logiciel",
          moisExperiencefin: 4,
          moisExperiencedeb: 6,
          technique: " ",
          description:
            "Recherche de solutions répondant au besoin de refonte de l’application d'un référentiel de livraison puis développement d’un POC pour la solution choisie \n\n- Analyse de l’existant\n- Benchmark et qualification des autres solutions du marché (Nexus, Artifactory)\n- Développement d’un POC\n\nEnvironnement technique : JAVA/JEE (JPA2, WICKET, Spring3), JonAS 5.2, PostgreSQL 9, Web Services (SOAP/REST), Maven, Nexus, Artifactory\n",
          poste: "Architecte logiciel",
          anneeCourante: false,
          anneesExperiencedeb: 2013,
        },
        {
          entreprise: "HP Bangalore - india",
          anneesExperiencefin: 2011,
          projet: " ",
          moisExperiencefin: 6,
          moisExperiencedeb: 1,
          technique: " ",
          description:
            "Support, formation et suivi des équipes (rétro-ingénierie PACBASE/J2EE, Approche MDA)\n\nAide à la Mise en place d’une démarche agile basée sur SCRUM",
          poste: "Coaching Scrum de 4 équipes de développement ",
          anneeCourante: false,
          anneesExperiencedeb: 2011,
        },
        {
          entreprise: "WellMark",
          anneesExperiencefin: 2011,
          projet: "Modernisation d’une application de gestion des couvertures maladie",
          moisExperiencefin: 12,
          moisExperiencedeb: 1,
          technique: " ",
          description:
            "Analyse technique et fonctionnelle du legacy (Cobol/Pacbase)\nConception, Modélisation et Développement (UML, JAVA, JEE) \nRéalisation de plugins Eclipse RCP\nMise en place d'outils de reporting avec BIRT",
          poste: "Développeur Java/JEE",
          anneeCourante: false,
          anneesExperiencedeb: 2010,
        },
      ],
    },
  ],
};
